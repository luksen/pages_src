---
title:  'Testpost'
subtitle: "Subtitling hard"
author: Lukas Senger
date: 2023-08-22
toc: true
license: '[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)'
---

# Very good stuff

lots of it

# Another chapter

## with subheading

and lots of text

# Possible features

 - automatically set post date (or at least check before commit)
 - automatically link to older versions of posts
