TMP_DIR := "tmp"
DEPLOY_DIR := "deploy"
PANDOC_DIR := "pandoc"
PANDOC_GLOBAL_DEFAULTS := "global.yaml"
STATIC_DIR := "static"
RESOURCE_DIR := PANDOC_DIR / "resources"

PANDOC_CMD := "pandoc --resource-path=" + RESOURCE_DIR + " --data-dir=" + PANDOC_DIR + " --defaults=" + PANDOC_GLOBAL_DEFAULTS

DEPLOY_BUILD_DIR := DEPLOY_DIR / "build"
DEPLOY_GIT_DIR := DEPLOY_DIR / "git"

default:
	just --list

copy-static:
	cp -r {{STATIC_DIR}} {{DEPLOY_BUILD_DIR}}/

_build_dir dir:
	#!/usr/bin/bash
	set -euo pipefail
	echo Building {{dir}}...
	mkdir -p {{TMP_DIR}}
	echo "" > {{TMP_DIR}}/{{dir}}_index.md
	echo "# {{dir}}" >> {{TMP_DIR}}/{{dir}}_index.md
	for file in `find {{dir}} -name '*.md' | rev | cut -d '.' -f 2 | rev | tr '\n' ' '`
	do
		mkdir -p {{DEPLOY_BUILD_DIR}}/$(dirname $file)
		echo "  Building {{DEPLOY_BUILD_DIR}}/${file}.html"
		{{PANDOC_CMD}} --defaults={{dir}} -o {{DEPLOY_BUILD_DIR}}/"${file}.html" "${file}.md"
		{{PANDOC_CMD}} --defaults={{dir}}_index -t markdown --variable "index_link=${file}.html" "${file}.md" >> {{TMP_DIR}}/{{dir}}_index.md
	done

_build_resources:
	#!/usr/bin/bash
	set -euo pipefail
	echo Building resources...
	for file in `find {{RESOURCE_DIR}} -name '*.md' | rev | cut -d '.' -f 2 | rev | tr '\n' ' '`
	do
		echo "  Building ${file}.html"
		pandoc -o "${file}.html" "${file}.md"
	done

build: _build_resources (_build_dir "Posts") && copy-static 
	#!/usr/bin/bash
	set -euo pipefail
	echo Building index...
	rm -f {{TMP_DIR}}/generated_index.md
	cat index.md {{TMP_DIR}}/*_index.md > {{TMP_DIR}}/generated_index.md
	echo "  Building {{DEPLOY_BUILD_DIR}}/index.html"
	{{PANDOC_CMD}} --defaults=index -o "{{DEPLOY_BUILD_DIR}}/index.html" "{{TMP_DIR}}/generated_index.md"

open: build
	firefox {{DEPLOY_BUILD_DIR}}/index.html

add: build
	#!/usr/bin/bash
	set -euo pipefail
	cd {{DEPLOY_GIT_DIR}}
	git rm -qrf -- *
	mv ../../{{DEPLOY_BUILD_DIR}}/* .
	git add -A
	git commit || echo "Nothing to commit."

commit: add
	#!/usr/bin/bash
	set -euo pipefail
	git commit || echo "Nothing to commit."

review: commit
	cd {{DEPLOY_GIT_DIR}} && git show

deploy: commit
	cd {{DEPLOY_GIT_DIR}} && git push
